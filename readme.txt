** Structure of the folder for use under Matlab **

The Matlab current folder you will use is the folder 'Envnmt'

	*Envnmt*


REA_EC_X files are the main files to calculate the oxygen fluxes by EC and REA.

They are campaign-specific to improve readability in the code because each campaign has slightly different conditions.


deployment_parameter_X files are called by the REA_EC_X file and provide deployment information.


deployment_parameter is a file used in the very first versions, it is not meant to be used as such, however it contains deployment information for all passed campaigns (before spring 2021).


All folders are added in the REA_EC_X files search path in the beginning of the script (they should be added if it is not the case)
-> despiking 
-> oxygen compensation for conversion from dphi to mmol/m3
-> figures for bar charts, saving figures
-> spectrum to plot spectra
-> functions for all the function called in the REA_EC_X file

	*Campagnes*

Contains the folders with the data files for each campaign 
.txt files are oxygen data retrieved from the micro-opcodes 
.dat .sen .hdr files are velocity data from the ADV
.csv files are velocity data from the automate

Subfolders are created here to save figures from the script

